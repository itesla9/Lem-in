/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   magic.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 14:07:01 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/28 19:19:12 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

char	*ft_line4new(char *s, t_box *a)
{
	char	*line;
	int		marker;
	int		i;

	i = 0;
	line = NULL;
	marker = ft_atoi(s);
	while (i < a->rooms)
	{
		if (a->p[i].num == marker)
		{
			line = ft_strdup(a->p[i].name);
			break ;
		}
		i++;
	}
	return (line);
}

void	ft_make_ss(t_box *a, char **new, int l)
{
	int i;
	int size;

	size = ft_count_double(new);
	a->ss[l] = (t_e *)malloc(sizeof(t_e) * size);
	i = 0;
	while (i < size)
	{
		a->ss[l][i].ant = 0;
		a->ss[l][i].name = new[i];
		a->ss[l][i].length = size;
		i++;
	}
}

void	ft_make_changes(char **mass, t_box *a)
{
	char		**new;
	int			size;
	int			i;
	static int	l;

	size = ft_count_double(mass);
	new = (char **)malloc(sizeof(char *) * (size + 1));
	i = 0;
	while (mass[i])
	{
		new[i] = ft_line4new(mass[i], a);
		i++;
	}
	new[i] = 0;
	ft_make_ss(a, new, l);
	l++;
	//ft_free(new);
	ft_free(mass);
}

void	ft_change_names(t_box *a)
{
	int		size;
	int		i;
	char	**mass;

	i = 0;
	size = ft_count_double(a->fresh);
	a->ss = (t_e**)malloc(sizeof(t_e*) * (size + 1));
	a->ss[size] = 0;
	while (i < size)
	{
		mass = ft_strsplit(a->fresh[i], '-');
		ft_make_changes(mass, a);
		i++;
	}
}
