/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   x.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:51:31 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/02 13:13:13 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	ft_width_bigger_precision_o(t_struct *ps, char *fresh, int len)
{
	int	reserve_int;
	int reserve;

	if (fresh[0] == '0')
		len = 0;
	reserve = ft_reserve_full(ps, len);
	if (ps->shark == 1)
		reserve = reserve - 1;
	if (ps->precision > len)
		reserve_int = ps->precision - len;
	else
		reserve_int = 0;
	if (ps->precision != -1)
	{
		if (ps->precision > len)
			ft_hyphen1_0_o(ps, fresh, reserve, reserve_int);
		if (ps->precision <= len && ps->precision >= 0)
			ft_hyphen2_0_o(ps, fresh, reserve);
	}
	if (ps->precision == -1)
		ft_hyphen3_0_o(ps, fresh, reserve);
}

void	ft_width_less_precision_o(t_struct *ps, char *fresh, int len)
{
	int repeat;

	repeat = 0;
	if (ps->precision > len)
		repeat = ps->precision - len;
	if (ps->shark == 1)
	{
		ft_putchar('0');
		repeat = repeat - 1;
	}
	if (fresh[0] == '0')
		return ;
	if (ps->precision > len)
	{
		while (repeat > 0)
		{
			ft_putchar('0');
			repeat--;
		}
		ft_putstr(fresh);
	}
	else
		ft_putstr(fresh);
}
