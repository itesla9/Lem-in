/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/28 19:00:40 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/02 20:22:25 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	lessword(char *zzz, int residue)
{
	int	i;

	i = 0;
	while (i < residue)
	{
		ft_putchar(zzz[i]);
		i++;
	}
}

void	ft_s1_1(t_struct *ps, int reserve, int residue, char *zzz)
{
	if (ps->hyphen == 1)
	{
		lessword(zzz, residue);
		ft_extra_add(ps, reserve);
	}
	if (ps->hyphen == 0)
	{
		ft_extra_add(ps, reserve);
		lessword(zzz, residue);
	}
}

void	ft_s1(t_struct *ps, int len, int residue, char *zzz)
{
	int	reserve;

	reserve = ps->width - residue;
	if (ps->precision < len)
		ft_s1_1(ps, reserve, residue, zzz);
	else
	{
		if (ps->hyphen == 1)
		{
			ft_putstr(zzz);
			ft_extra_add(ps, reserve);
		}
		if (ps->hyphen == 0)
		{
			ft_extra_add(ps, reserve);
			ft_putstr(zzz);
		}
	}
}

void	ft_s(t_struct *ps, va_list ap)
{
	char	*zzz;
	int		len;
	int		residue;
	char	*zzz2;

	zzz2 = "(null)";
	zzz = va_arg(ap, char*);
	if (zzz == NULL)
		zzz = zzz2;
	residue = 0;
	len = ft_strlen2(zzz);
	if (ps->precision != -1 && ps->precision <= len)
		residue = ps->precision;
	if (ps->precision > len || ps->precision == -1)
		residue = len;
	if (ps->width >= residue)
		ft_s1(ps, len, residue, zzz);
	else
	{
		if (ps->precision < len)
			lessword(zzz, residue);
		else
			ft_putstr(zzz);
	}
}
