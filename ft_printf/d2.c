/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   d2.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/30 16:54:33 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/01 15:18:41 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

unsigned long long	ft_put_sign(t_struct *ps, long long xxx)
{
	unsigned long long yyy;

	if (ps->space == 1 && xxx >= 0)
		ft_putchar(' ');
	if (ps->plus == 1 && xxx >= 0)
		ft_putchar('+');
	if (xxx < 0)
	{
		ft_putchar('-');
		yyy = -xxx;
	}
	else
		yyy = xxx;
	return (yyy);
}

void				ft_putspace(int reserve, t_struct *ps)
{
	while (reserve > 0)
	{
		if (ps->precision == -1 && ps->zero == 1)
			ft_putchar('0');
		else
			ft_putchar(' ');
		reserve--;
	}
}

void				ft_hyphen1_0(t_struct *ps, long long xxx, int reserve,
								int r_int)
{
	unsigned long long yyy;

	if (ps->hyphen == 1)
	{
		yyy = ft_put_sign(ps, xxx);
		while (r_int > 0)
		{
			ft_putchar('0');
			r_int--;
		}
		ft_putnbr2(yyy, ps);
		ft_putspace(reserve, ps);
	}
	if (ps->hyphen == 0)
	{
		ft_putspace(reserve, ps);
		yyy = ft_put_sign(ps, xxx);
		while (r_int > 0)
		{
			ft_putchar('0');
			r_int--;
		}
		ft_putnbr2(yyy, ps);
	}
}

void				ft_hyphen2_0(t_struct *ps, long long xxx, int reserve)
{
	unsigned long long	yyy;

	if (ps->hyphen == 1)
	{
		yyy = ft_put_sign(ps, xxx);
		ft_putnbr2(yyy, ps);
		ft_putspace(reserve, ps);
	}
	if (ps->hyphen == 0)
	{
		ft_putspace(reserve, ps);
		yyy = ft_put_sign(ps, xxx);
		ft_putnbr2(yyy, ps);
	}
}

void				ft_hyphen3_0(t_struct *ps, long long xxx, int reserve,
								unsigned long long yyy)
{
	if (ps->zero == 0)
	{
		if (ps->hyphen == 0)
		{
			ft_putspace(reserve, ps);
			yyy = ft_put_sign(ps, xxx);
			ft_putnbr2(yyy, ps);
		}
		if (ps->hyphen == 1)
		{
			yyy = ft_put_sign(ps, xxx);
			ft_putnbr2(yyy, ps);
			ft_putspace(reserve, ps);
		}
	}
	if (ps->zero == 1)
	{
		if (ps->hyphen == 0)
		{
			yyy = ft_put_sign(ps, xxx);
			ft_putspace(reserve, ps);
			ft_putnbr2(yyy, ps);
		}
	}
}
