/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   d.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/18 17:00:04 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/01 17:25:40 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int			ft_reserve(t_struct *ps, long long xxx, int len)
{
	int	reserve;

	if (ps->precision == 0 && xxx == 0)
		len = 0;
	if (ps->precision > len)
	{
		if ((ps->plus == 0 && ps->space == 0) && xxx >= 0)
			reserve = ps->width - ps->precision;
		if ((ps->plus == 1 && xxx >= 0) || xxx < 0)
			reserve = ps->width - ps->precision - 1;
		if ((ps->space == 1 && xxx >= 0))
			reserve = ps->width - ps->precision - 1;
	}
	else
	{
		if ((ps->plus == 0 && ps->space == 0) && xxx >= 0)
			reserve = ps->width - len;
		if ((ps->plus == 1 && xxx >= 0) || xxx < 0)
			reserve = ps->width - len - 1;
		if ((ps->space == 1 && xxx >= 0))
			reserve = ps->width - len - 1;
	}
	return (reserve);
}

void		ft_width_bigger_precision(t_struct *ps, long long xxx, int len)
{
	int					r_int;
	int					reserve;
	unsigned long long	yyy;

	yyy = 0;
	if (ps->precision > len)
		r_int = ps->precision - len;
	else
		r_int = 0;
	if (ps->precision != -1)
	{
		if (ps->hyphen == 0)
			ps->space = 0;
		reserve = ft_reserve(ps, xxx, len);
		if (ps->precision > len)
			ft_hyphen1_0(ps, xxx, reserve, r_int);
		if (ps->precision <= len && ps->precision >= 0)
			ft_hyphen2_0(ps, xxx, reserve);
	}
	reserve = ft_reserve(ps, xxx, len);
	if (ps->precision == -1)
		ft_hyphen3_0(ps, xxx, reserve, yyy);
}

void		ft_width_less_precision(t_struct *ps, long long xxx, int len)
{
	int					repeat;
	unsigned long long	yyy;

	yyy = ft_put_sign(ps, xxx);
	if (ps->precision > len)
	{
		repeat = ps->precision - len;
		while (repeat > 0)
		{
			ft_putchar('0');
			repeat--;
		}
		ft_putnbr2(yyy, ps);
	}
	else
		ft_putnbr2(yyy, ps);
}

int			ft_len(long long xxx)
{
	int i;
	int j;

	i = 0;
	j = 0;
	if (xxx == 0)
		j++;
	while (xxx != 0)
	{
		i++;
		xxx /= 10;
	}
	return (i + j);
}

void		ft_d(t_struct *ps, va_list ap)
{
	long long	xxx;
	int			len;

	xxx = va_arg(ap, int);
	len = ft_len(xxx);
	if (ps->width <= ps->precision)
		ft_width_less_precision(ps, xxx, len);
	else
		ft_width_bigger_precision(ps, xxx, len);
}
