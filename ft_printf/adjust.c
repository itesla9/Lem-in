/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   adjust.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 10:50:41 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/04 14:23:33 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

unsigned long long		mod_add(t_struct *ps, unsigned long long xxx,
							va_list ap)
{
	if (ps->a == 6)
		xxx = va_arg(ap, uintmax_t);
	if (ps->a == 5)
		xxx = va_arg(ap, unsigned long long);
	if (ps->a == 4)
		xxx = va_arg(ap, unsigned long);
	if (ps->a == 3)
		xxx = va_arg(ap, size_t);
	if (ps->a == 2)
		xxx = (unsigned short int)va_arg(ap, int);
	if (ps->a == 1)
		xxx = (unsigned char)va_arg(ap, int);
	return (xxx);
}

void					ft_o(t_struct *ps, va_list ap)
{
	unsigned long long	xxx;
	int					len;
	char				*fresh;

	xxx = 0;
	if (*(ps->ptr) == 'O')
		xxx = va_arg(ap, unsigned long);
	if (ps->a == 0 && *(ps->ptr) == 'o')
		xxx = (unsigned int)va_arg(ap, int);
	if (ps->a != 0 && *(ps->ptr) == 'o')
		xxx = mod_add(ps, xxx, ap);
	fresh = ft_itoa_base2(xxx, 8);
	len = ft_strlen2(fresh);
	if (ps->width <= ps->precision)
		ft_width_less_precision_o(ps, fresh, len);
	else
		ft_width_bigger_precision_o(ps, fresh, len);
	free(fresh);
}

void					ft_x(t_struct *ps, va_list ap)
{
	unsigned long long	xxx;
	int					len;
	char				*fresh;

	xxx = 0;
	len = 0;
	if (ps->a == 0)
		xxx = (unsigned int)va_arg(ap, int);
	else
		xxx = mod_add(ps, xxx, ap);
	fresh = ft_itoa_base2(xxx, 16);
	if (*(ps->ptr) == 'x')
		fresh = ft_magic(fresh);
	len = ft_strlen2(fresh);
	if (ps->width <= ps->precision)
		ft_width_less_precision_x(ps, fresh, len);
	else
		ft_width_bigger_precision_x(ps, fresh, len);
	free(fresh);
}

void					ft_u(t_struct *ps, va_list ap)
{
	unsigned long long	xxx;
	int					len;

	xxx = 0;
	if (*(ps->ptr) == 'U')
		xxx = (unsigned long)va_arg(ap, long);
	if (*(ps->ptr) == 'u' && ps->a == 0)
		xxx = (unsigned int)va_arg(ap, int);
	if (*(ps->ptr) == 'u' && ps->a != 0)
		xxx = mod_add(ps, xxx, ap);
	len = ft_len_base2(xxx, 10);
	if (ps->width <= ps->precision)
		ft_widthles_und(ps, xxx, len);
	else
		ft_widthbig_und(ps, xxx, len);
}

void					ft_adjust(t_struct *ps, va_list ap)
{
	char *tmp;

	tmp = ps->ptr;
	if (ps->a != 0 && (*tmp == 'd' || *tmp == 'i'))
		ft_d2(ps, ap);
	if (*tmp == 's' || *tmp == 'S')
		ft_s(ps, ap);
	if (*tmp == 'c' || *tmp == 'C')
		ft_c(ps, ap);
	if ((*tmp == 'd' || *tmp == 'i') && ps->a == 0)
		ft_d(ps, ap);
	if (*tmp == 'D')
		ft_d2(ps, ap);
	if (*tmp == 'U' || *tmp == 'u')
		ft_u(ps, ap);
	if (*tmp == 'x' || *tmp == 'X')
		ft_x(ps, ap);
	if (*tmp == 'o' || *tmp == 'O')
		ft_o(ps, ap);
	if (*tmp == 'p')
		ft_p(ps, ap);
	if (*tmp == 'n')
		ft_n(ap);
}
