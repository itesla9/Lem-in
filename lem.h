/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem-in.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 12:18:23 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/28 20:03:17 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_H
# define LEM_H

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include "libft/libft.h"
# include "get_next_line.h"
# include "ft_printf/header.h"

typedef struct	s_e
{
	int			ant;
	char		*name;
	int			length;
}				t_e;

typedef struct	s_d
{
	int			num;
	struct s_d	*next;
}				t_links;

typedef	struct	s_c
{
	int			y;
	int			x;
	struct s_c	*next;
}				t_l;

typedef	struct	s_b
{
	int			num;
	char		*name;
}				t_rooms;

typedef struct	s_a
{
	int			anewsize;
	int			tofind;
	int			size4magic;
	size_t		w;
	int			getcheck;
	int			flag;
	int			ants;
	int			error;
	int			start;
	int			end;
	char		**names;
	char		**coord;
	char		**chief;
	char		**links;
	char		*startname;
	char		*endname;
	int			**matrix;
	t_rooms		*p;
	int			rooms;
	t_l			*xy;
	t_l			*b;
	t_links		*begin;
	char		*s;
	char		*res;
	char		*shark;
	char		*r;
	char		**final;
	char		**fresh;
	t_l			*z;
	t_e			**ss;
	int			fin;
	int			counter;
}				t_box;

char			**ft_split(char *s);
char			**ft_make_chief(t_box *a, char *line);
int				ft_count_line_mass(char *line);
void			ft_check_line(t_box *a, char *line);
t_box			*ft_make_a(void);
char			*ft_epur(char *s);
void			ft_second_step(t_box *a);
void			ft_check_next_line(char *s, t_box *a);
char			**ft_make_anew_names(t_box *a, char **mass);
char			**ft_make_anew_coord(t_box *a, char **mass);
char			**ft_make_anew_links(t_box *a, char *line);
int				ft_count_double(char **mass);
void			ft_compare(t_box *a);
void			ft_printf_mass(char **mass);
void			ft_check_links(t_box *a);
int				ft_check_name(char **mass, char *s);
void			ft_find_links(t_box *a);
char			**ft_copy_links(t_box *a);
void			ft_fill_rooms(t_box *a, char **copy_names);
void			ft_make_matrix(t_box *a);
void			ft_make_list(t_box *a);
char			**ft_copy_mass(char **mass);
int				ft_links3(t_box *a, char **mass);
void			ft_fill_matrix(t_box *a);
void			ft_printf_matrix(t_box *a);
void			ft_make_links(t_box *a);
t_links			*ft_magic_add(t_box *a, t_links *tmp, int j);
t_links			*ft_add_new_cell(t_links *tmp, t_box *a, int i, int j);
char			*ft_optimus(t_box *a, int i, int j);
void			ft_make_beauty(t_box *a);
void			ft_change_names(t_box *a);
void			ft_find_decision(t_box *a);
char			**ft_truecopy_mass(char **mass);
void			ft_printf_mass(char **mass);
int				ft_isnumber(char *s);
void			ft_make_fresh(t_box *a, char *s);
void			ft_change_matrix(t_box *a, char *s);
int				ft_checker(char *s, int j);
void			ft_make_optimal(t_box *a);
void			ft_makefull(t_box *a);
void			ft_clearfull(t_box *a);
int				ft_links2(t_box *a, char **copy);
void			ft_check_links(t_box *a);
void			ft_magic_lem(int i, int j, t_box *a, int k);
void			ft_free(char **mass);
int				ft_check4xy(char *cc, char *s, t_box *a);
char            *ft_strjoin_free(char const *s1, char const *s2);
void			ft_free_all(t_box *a);
#endif
