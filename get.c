/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/17 19:08:56 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/22 12:33:54 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

int		ft_length(char *s)
{
	int i;

	i = 0;
	while (s[i])
		i++;
	return (i);
}

char	*ft_make_line(char *s, char letter)
{
	char	*buf;
	int		i;

	i = 0;
	if (s == 0)
	{
		buf = (char*)malloc(sizeof(char) * 2);
		buf[0] = letter;
		buf[1] = 0;
		return (buf);
	}
	else
	{
		buf = (char*)malloc(sizeof(char) * (ft_length(s) + 2));
		while (s[i])
		{
			buf[i] = s[i];
			i++;
		}
		buf[i] = letter;
		buf[++i] = 0;
	}
	free(s);
	return (buf);
}

char	*lines(int fd, char *s)
{
	char	letter;
	char	*extra;

	extra = (char *)malloc(sizeof(char) * 3);
	extra[2] = 0;
	extra = "-1";
	while (read(fd, &letter, 1) && letter != '\n')
	{
		if (!(s = ft_make_line(s, letter)))
			return (0);
	}
	if (letter == '\n' && s == 0)
		return (extra);
	return (s);
}

int		get_next_line(int fd, char **line)
{
	char	*s;

	s = NULL;
	if (line == NULL)
		return (-1);
	*line = NULL;
	if (!(s = lines(fd, s)))
		return (0);
	if (ft_atoi(s) == -1)
		return (-1);
	*line = s;
	if (s == NULL)
		return (0);
	return (1);
}
