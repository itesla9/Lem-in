/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlinks.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 15:59:31 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/29 17:24:19 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

char	*ft_optimus(t_box *a, int i, int j)
{
	char *fresh;

	fresh = ft_itoa(a->matrix[i][j]);
	return (fresh);
}

void	ft_make_shark(t_box *a)
{
	a->shark = (char *)malloc(sizeof(char) * 2);
	a->shark[0] = '-';
	a->shark[1] = 0;
}

void	ft_final_final(t_box *a)
{
	ft_printf("%d\n", a->ants);
	ft_printf_mass(a->chief);
	ft_printf_mass(a->links);
	ft_printf("\n");
	ft_find_decision(a);
}

void	ft_make_links(t_box *a)
{
	int size;

	a->size4magic = a->rooms;
	a->tofind = a->matrix[0][a->rooms];
	ft_makefull(a);
	ft_make_shark(a);
	ft_magic_lem(1, 1, a, 0);
	size = ft_count_double(a->final);
	ft_clearfull(a);
	ft_makefull(a);
	while (size > 0)
	{
		ft_magic_lem(1, 1, a, 0);
		ft_make_optimal(a);
		ft_clearfull(a);
		ft_makefull(a);
		size--;
	}
	if (a->fresh == NULL)
		a->error = 1;
	if (a->error != 1)
		ft_change_names(a);
	if (a->error != 1)
		ft_final_final(a);
}
