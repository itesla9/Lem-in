/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillmatrix.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 13:59:09 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/28 20:48:33 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

void	ft_printf_mass(char **mass)
{
	int	i;

	i = 0;
	while (mass[i])
	{
		ft_printf("%s\n", mass[i]);
		i++;
	}
}

int		ft_isnumber(char *s)
{
	int i;

	i = 0;
	while (s[i])
	{
		if (!(s[i] >= '0' && s[i] <= '9'))
			return (1);
		i++;
	}
	return (0);
}

void	ft_fill_matrix(t_box *a)
{
	t_l *tmp;

	tmp = a->xy;
	while (tmp)
	{
		a->matrix[tmp->y][tmp->x] = 1;
		a->matrix[tmp->x][tmp->y] = 1;
		tmp = tmp->next;
	}
}

void	ft_makefull(t_box *a)
{
	a->s = (char *)malloc(sizeof(char) * 2);
	a->s[0] = '1';
	a->s[1] = 0;
	a->final = (char**)malloc(sizeof(char *) * 2);
	a->final[0] = ft_strdup(a->s);
	a->final[1] = 0;
	free(a->s);
}

void	ft_clearfull(t_box *a)
{
	ft_free(a->final);
	a->final = NULL;
}
