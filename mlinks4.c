/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlinks4.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/21 17:47:55 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/28 20:44:30 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

char	**ft_magic_lem2(t_box *a, int k, int j)
{
	char	**anew;

	a->anewsize = ft_count_double(a->final);
	anew = (char **)malloc(sizeof(char *) * (a->anewsize + 2));
	a->s = ft_strjoin(a->final[k], a->shark);
	a->s = ft_strjoin_free(a->s, ft_optimus(a, 0, j));
	return (anew);
}

void	ft_magic_lem3(t_box *a, char **anew)
{
	ft_free(a->final);
	a->final = anew;
}

void	ft_magic_lem(int i, int j, t_box *a, int k)
{
	char	**anew;
	int		z;

	if (a->matrix[i][j] == a->tofind)
		return ;
	j = 1;
	while (j <= a->size4magic)
	{
		if (a->matrix[i][j] == 1 && ft_checker(a->final[k], j) == 0)
		{
			z = 0;
			anew = ft_magic_lem2(a, k, j);
			while (z < a->anewsize)
			{
				anew[z] = ft_strdup(a->final[z]);
				z++;
			}
			anew[z++] = ft_strdup(a->s);
			free(a->s);
			anew[z] = 0;
			ft_magic_lem3(a, anew);
			ft_magic_lem(j, 0, a, z - 1);
		}
		j++;
	}
}

void	ft_check_links(t_box *a)
{
	char	**copy;

	copy = ft_truecopy_mass(a->links);
	if (ft_links2(a, copy) == 1)
		a->error = 1;
	ft_free(copy);
}
