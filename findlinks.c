/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   findlinks.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 14:00:24 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/28 20:28:42 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

int		ft_count_names(t_box *a)
{
	int	i;
	int	size;

	i = 0;
	size = 0;
	while (a->names[i])
	{
		if (ft_strcmp(a->names[i], a->startname) != 0 &&
			ft_strcmp(a->names[i], a->endname) != 0)
			size++;
		i++;
	}
	return (size);
}

char	**ft_copy_names(t_box *a)
{
	char	**copy;
	int		size;
	int		i;
	int		j;

	i = 0;
	j = 0;
	size = ft_count_names(a);
	copy = (char **)malloc(sizeof(char *) * (size + 1));
	copy[size] = 0;
	size = ft_count_double(a->names);
	while (i < size)
	{
		if (ft_strcmp(a->names[i], a->startname) != 0 &&
			ft_strcmp(a->names[i], a->endname) != 0)
		{
			copy[j] = ft_strdup(a->names[i]);
			j++;
		}
		i++;
	}
	return (copy);
}

void	ft_find_links(t_box *a)
{
	char	**names2;
	int		i;

	i = 0;
	names2 = ft_copy_names(a);
	a->rooms = ft_count_double(names2) + 2;
	a->p = (t_rooms *)malloc(sizeof(t_rooms) * a->rooms);
	ft_fill_rooms(a, names2);
	ft_make_matrix(a);
	ft_free(names2);
}

void	ft_compare(t_box *a)
{
	int		i;
	int		j;
	char	**p;

	i = 0;
	j = 1;
	while (a->coord[i] && a->coord[i + j] != NULL)
	{
		p = a->coord;
		while (p[i + j])
		{
			if (ft_strcmp(a->coord[i], p[i + j]) == 0)
			{
				a->error = 1;
				return ;
			}
			j++;
		}
		j = 1;
		i++;
	}
}

int		ft_check_name(char **mass, char *s)
{
	int	i;

	i = 0;
	while (mass[i])
	{
		if (ft_strcmp(mass[i], s) == 0)
			return (1);
		i++;
	}
	return (0);
}
