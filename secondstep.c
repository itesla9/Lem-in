/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   secondstep.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 15:28:13 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/29 19:26:48 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

int		ft_check_after_end(char *s, t_box *a)
{
	char **mass;

	if (s == NULL)
		return (-1);
	mass = ft_strsplit(s, ' ');
	if (s[0] == '#' || s[0] == 'L')
	{
		ft_free(mass);
		return (-1);
	}
	if (ft_count_line_mass(s) < 3)
	{
		ft_free(mass);
		return (-1);
	}
	if (ft_isnumber(mass[1]) == 1)
	{
		ft_free(mass);
		return (-1);
	}
	if (ft_isnumber(mass[2]) == 1)
	{
		ft_free(mass);
		return (-1);
	}
	a->endname = ft_strdup(mass[0]);
	ft_free(mass);
	return (a->end + 1);
}

int		ft_check_after_start(char *s, t_box *a)
{
	char **mass;

	if (s == NULL)
		return (-1);
	mass = ft_strsplit(s, ' ');
	if (s[0] == '#' || s[0] == 'L')
	{
		ft_free(mass);
		return (-1);
	}
	if (ft_count_line_mass(s) < 3)
	{
		ft_free(mass);
		return (-1);
	}
	if (ft_isnumber(mass[1]) == 1)
	{
		ft_free(mass);
		return (-1);
	}
	if (ft_isnumber(mass[2]) == 1)
	{
		ft_free(mass);
		return (-1);
	}
	a->startname = ft_strdup(mass[0]);
	ft_free(mass);
	return (a->start + 1);
}

int		ft_make_names_coord(char *s, t_box *a, int counter)
{
	char **mass;

	mass = ft_strsplit(s, ' ');
	counter++;
	if (ft_count_line_mass(s) != 3)
	{
		ft_free(mass);
		return (counter);
	}
	if (a->names != NULL)
	{
		a->coord = ft_make_anew_coord(a, mass);
		a->names = ft_make_anew_names(a, mass);
	}
	if (a->names == NULL)
	{
		a->names = (char **)malloc(sizeof(char *) * 2);
		a->coord = (char **)malloc(sizeof(char *) * 2);
		a->names[0] = ft_strdup(mass[0]);
		a->names[1] = 0;
		a->coord[0] = ft_strjoin(mass[1], mass[2]);
		a->coord[1] = 0;
	}
	ft_free(mass);
	return (counter);
}

void	ft_start_end_coord(t_box *a)
{
	int i;
	int counter;
	int size;

	i = 0;
	size = 0;
	counter = 0;
	while (a->chief[i])
	{
		if (ft_strcmp(a->chief[i], "##start") == 0 && a->start != -1)
			a->start = ft_check_after_start(a->chief[i + 1], a);
		if (ft_strcmp(a->chief[i], "##end") == 0 && a->end != -1)
			a->end = ft_check_after_end(a->chief[i + 1], a);
		if (a->chief[i][0] != '#' && a->chief[i][0] != 'L')
			counter = ft_make_names_coord(a->chief[i], a, counter);
		i++;
	}
	if (a->start != 1 || a->end != 1)
		a->error = 1;
	if (a->error != 1)
		size = ft_count_double(a->names);
	if (size != counter)
		a->error = 1;
	if (a->error != 1)
		ft_compare(a);
}

void	ft_second_step(t_box *a)
{
	if (a->chief == NULL || a->links == NULL)
		a->error = 1;
	if (a->error != 1)
		ft_start_end_coord(a);
	if (a->error != 1)
		ft_check_links(a);
	if (a->error != 1)
		ft_find_links(a);
	if (a->error != 1)
		ft_make_list(a);
	if (a->error != 1)
		ft_fill_matrix(a);
	if (a->error != 1)
		ft_make_links(a);

}
