/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/24 20:01:35 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/29 20:06:31 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

void	ft_free_list(t_l *start)
{
	t_l *tmp;
	t_l *ptr;

	tmp = start;	
	while (tmp)
	{
		ptr = tmp;
		tmp = tmp->next;
		free(ptr);
	}
}		

void	ft_free(char **mass)
{
	int i;

	i = 0;
	while (mass[i])
	{
		free(mass[i]);
		i++;
	}
	free(mass);
}

char            *ft_strjoin_free(char const *s1, char const *s2)
{
    char    *fresh;
    int     i;
    int     j;

    i = 0;
    j = 0;
    if (!s1 || !s2)
        return (NULL);
    fresh = (char *)malloc(ft_strlen(s1) + ft_strlen(s2) + 1);
    if (!fresh)
        return (NULL);
    while (s1[i])
    {
        fresh[i] = s1[i];
        i++;
    }
    while (s2[j])
    {
        fresh[i] = s2[j];
        i++;
        j++;
    }
    fresh[i] = '\0';
	free((void*)s1);
	free((void*)s2);
    return (fresh);
}

void    ft_free_all(t_box *a)
{
    if (a->names != NULL)
        ft_free(a->names);
    if (a->coord != NULL)
        ft_free(a->coord);
    if (a->chief != NULL)
        ft_free(a->chief);
    if (a->links != NULL)
        ft_free(a->links);
    if (a->final != NULL)
        ft_free(a->final);
    if (a->fresh != NULL)
        ft_free(a->fresh);
    if (a->xy != NULL)
        ft_free_list(a->xy);
}
