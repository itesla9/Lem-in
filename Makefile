#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/21 18:47:26 by yteslenk          #+#    #+#              #
#    Updated: 2017/03/24 20:03:25 by yteslenk         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = lem-in

GCC = gcc -Wall -Wextra -Werror

SRC =   main.c \
        b.c \
        c.c \
        checklinks.c \
        decision.c \
        epur.c \
        fillmatrix.c \
        findlinks.c \
        get.c \
        magic.c \
        mlinks.c \
        mlinks2.c \
        mlinks3.c \
        mlinks4.c \
        mlist.c \
        room.c \
        secondstep.c \
		free.c

OBJ     =   $(SRC:.c=.o)

all:    $(NAME)

$(NAME): $(OBJ)
		cd libft/ && $(MAKE)
		cd ft_printf/ && $(MAKE)
		gcc -o $(NAME) $(OBJ) ft_printf/libftprintf.a libft/libft.a

%.o : %.c
		$(GCC) -c -o  $@ $<

clean:
		rm -f $(OBJ)
		make clean -C libft/
		make clean -C ft_printf/

fclean: clean
		make fclean -C libft/
		make fclean -C ft_printf/
		rm -f $(NAME)

re: fclean all