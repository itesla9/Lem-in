/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   room.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 15:09:59 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/28 20:27:27 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

int		*ft_matrix_line(t_box *a)
{
	int	i;
	int size;
	int *fresh;

	i = 0;
	size = a->rooms + 1;
	fresh = (int *)malloc(sizeof(int) * size);
	while (i < size)
	{
		fresh[i] = 0;
		i++;
	}
	return (fresh);
}

void	ft_printf_matrix(t_box *a)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (i < a->rooms + 1)
	{
		j = 0;
		while (j < a->rooms + 1)
		{
			ft_printf("%d", a->matrix[i][j]);
			j++;
		}
		ft_printf("\n");
		i++;
	}
}

void	ft_make_matrix(t_box *a)
{
	int size;
	int i;
	int j;

	i = 0;
	j = 0;
	size = a->rooms + 1;
	a->matrix = (int **)malloc(sizeof(int *) * (size));
	while (i < size)
	{
		a->matrix[i] = ft_matrix_line(a);
		i++;
	}
	a->matrix[0][0] = 9;
	i = 1;
	while (i < size)
	{
		a->matrix[0][i] = a->p[j].num;
		a->matrix[i][0] = a->p[j].num;
		i++;
		j++;
	}
}

void	ft_fill_rooms(t_box *a, char **names2)
{
	int i;
	int limit;
	int j;
	int z;

	limit = a->rooms - 1;
	i = 1;
	j = 0;
	z = 1;
	a->p[0].num = i;
	a->p[0].name = a->startname;
	a->p[limit].num = a->rooms;
	a->p[limit].name = a->endname;
	while (i < limit)
	{
		a->p[z].num = i + 1;
		a->p[z].name = ft_strdup(names2[j]);
		j++;
		z++;
		i++;
	}
}
