/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   epur.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 15:43:44 by yteslenk          #+#    #+#             */
/*   Updated: 2017/05/14 13:01:25 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

int		ft_full_size(char **mass)
{
	int i;
	int j;
	int	size;

	i = 0;
	size = 0;
	while (mass[i])
	{
		j = 0;
		while (mass[i][j])
		{
			size++;
			j++;
		}
		i++;
	}
	return (size);
}

char	*ft_epur_add(char *fresh, int size, char **mass, int f)
{
	int i;
	int j;
	int z;

	i = 0;
	z = 0;
	fresh = (char *)malloc(sizeof(char) * (f + size));
	while (mass[i])
	{
		j = 0;
		while (mass[i][j])
		{
			fresh[z] = mass[i][j];
			j++;
			z++;
		}
		if (mass[i + 1] != NULL)
		{
			fresh[z] = ' ';
			z++;
		}
		i++;
	}
	fresh[z] = 0;
	return (fresh);
}

char	*ft_epur(char *s)
{
	char	**mass;
	char	*fresh;
	int		size;
	int		f;

	fresh = NULL;
	mass = ft_strsplit(s, ' ');
	size = ft_count_double(mass);
	f = ft_full_size(mass);
	fresh = ft_epur_add(fresh, size, mass, f);
	if (mass != NULL)
		ft_free(mass);
	return (fresh);
}
