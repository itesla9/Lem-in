/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 14:14:27 by yteslenk          #+#    #+#             */
/*   Updated: 2017/05/14 12:42:57 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

void	ft_rooms(t_box *a, char *line)
{
	if (a->chief != NULL)
		a->chief = ft_make_chief(a, line);
	if (a->chief == NULL)
	{
		a->chief = (char **)malloc(sizeof(char *) * 2);
		a->chief[0] = ft_strdup(line);
		a->chief[1] = 0;
	}
}

void	ft_links(t_box *a, char *line)
{
	if (a->links != NULL)
		a->links = ft_make_anew_links(a, line);
	if (a->links == NULL)
	{
		a->links = (char **)malloc(sizeof(char *) * 2);
		a->links[0] = ft_strdup(line);
		a->links[1] = 0;
	}
}

void	ft_count_ants(t_box *a, char *line)
{
	a->ants = ft_atoi(line);
	a->flag++;
	if (a->ants <= 0)
		a->error = 1;
}

int		main(void)
{
	char	*line;
	t_box	*a;
	int		i;

	i = 0;
	a = ft_make_a();
	while ((a->getcheck = get_next_line(0, &line)) > 0)
	{
		if (a->error != 1 && a->flag == 1)
			ft_check_line(a, line);
		if (a->error != 1 && a->flag == 1)
			ft_rooms(a, line);
		if (a->error != 1 && a->flag > 1)
			ft_links(a, line);
		if (a->flag == 0)
			ft_count_ants(a, line);
			free(line);
	}
	if (a->error == 0 && a->ants == 0)
		a->error = 1;
	if (a->error != 1 && a->getcheck != -1)
		ft_second_step(a);
	if (a->getcheck == -1 || a->error == 1)
		ft_printf("error\n");
	ft_free_all(a);
	return (0);
}
