/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlist.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 14:30:46 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/25 19:10:50 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

int		ft_count_list(t_l *start)
{
	int i;
	t_l *tmp;

	i = 0;
	tmp = start;
	while (tmp)
	{
		i++;
		tmp = tmp->next;
	}
	return (i);
}

int		ft_cell(t_l *p)
{
	if (p->y == 0)
		return (2);
	if (p->x == 0)
		return (1);
	return (0);
}

t_l		*ft_xy_none_done(t_box *a, t_l *tmp)
{
	t_l *new;

	if (a->xy == NULL)
	{
		a->xy = (t_l*)malloc(sizeof(t_l));
		a->xy->y = 0;
		a->xy->x = 0;
		a->xy->next = NULL;
		tmp = a->xy;
	}
	else
	{
		new = (t_l*)malloc(sizeof(t_l));
		new->y = 0;
		new->x = 0;
		new->next = NULL;
		tmp->next = new;
		tmp = tmp->next;
	}
	return (tmp);
}

void	ft_make_xy(t_box *a, char *s)
{
	int			i;
	static t_l	*tmp;
	int			flag;

	flag = 0;
	if (a->xy == NULL)
	{
		tmp = ft_xy_none_done(a, tmp);
		flag++;
	}
	if (ft_cell(tmp) == 0 && flag == 0)
		tmp = ft_xy_none_done(a, tmp);
	i = 0;
	while (i < a->rooms)
	{
		if (ft_strcmp(a->p[i].name, s) == 0 && ft_cell(tmp) == 1)
			tmp->x = a->p[i].num;
		if (ft_strcmp(a->p[i].name, s) == 0 && ft_cell(tmp) == 2)
			tmp->y = a->p[i].num;
		i++;
	}
}

void	ft_make_list(t_box *a)
{
	char	**copy;
	int		size;
	int		i;
	int		j;
	int		flag;

	i = -1;
	j = 0;
	copy = ft_truecopy_mass(a->links);
	size = ft_count_double(copy);
	while (++i < size)
	{
		flag = 0;
		j = 0;
		while (flag != 2 && a->error != 1)
		{
			if (ft_check4xy(copy[i], a->names[j], a) == 1)
			{
				ft_make_xy(a, a->names[j]);
				flag++;
			}
			j++;
		}
	}
	ft_free(copy);
}
