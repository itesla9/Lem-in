/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checklinks.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 13:40:20 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/25 19:32:54 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

char	**ft_truecopy_mass(char **mass)
{
	char	**copy;
	int		size;
	int		i;
	int		z;
	int		f;

	size = 0;
	i = -1;
	z = 0;
	f = 0;
	while (mass[++i])
		mass[i][0] != '#' ? size++ : f++;
	copy = (char **)malloc(sizeof(char *) * (size + 1));
	copy[size] = 0;
	i = 0;
	while (i < size + f)
	{
		if (mass[i][0] != '#')
		{
			copy[z] = ft_strdup(mass[i]);
			z++;
		}
		i++;
	}
	return (copy);
}

char	**ft_copy_mass(char **mass)
{
	char	**copy;
	int		size;
	int		i;

	i = 0;
	size = ft_count_double(mass);
	copy = (char **)malloc(sizeof(char *) * (size + 1));
	copy[size] = 0;
	while (i < size)
	{
		copy[i] = ft_strdup(mass[i]);
		i++;
	}
	return (copy);
}

int		ft_links3(t_box *a, char **mass)
{
	int flag;
	int i;
	int j;

	i = 0;
	flag = 0;
	if (ft_count_double(mass) != 2)
		return (1);
	while (mass[i])
	{
		j = 0;
		while (a->names[j])
		{
			if (ft_strcmp(mass[i], a->names[j]) == 0)
			{
				flag++;
				break ;
			}
			j++;
		}
		i++;
	}
	if (flag != 2)
		return (1);
	return (0);
}

int		ft_links2(t_box *a, char **copy)
{
	int		i;
	int		j;
	int		flag;
	char	**mass;

	i = 0;
	j = 0;
	flag = 0;
	while (copy[i])
	{
		mass = ft_strsplit(copy[i], '-');
		if (ft_links3(a, mass) == 1)
		{
			ft_free(mass);
			return (1);
		}
		ft_free(mass);
		i++;
	}
	return (0);
}

int		ft_check4xy(char *cc, char *s, t_box *a)
{
	char	**mass;
	int		i;

	i = 0;
	mass = ft_strsplit(cc, '-');
	if (ft_strcmp(mass[0], mass[1]) == 0)
		a->error = 1;
	while (mass[i] && a->error != 1)
	{
		if (ft_strcmp(mass[i], s) == 0)
		{
			ft_free(mass);
			return (1);
		}
		i++;
	}
	ft_free(mass);
	return (0);
}
