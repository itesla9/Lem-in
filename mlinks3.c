/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlinks3.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/21 17:41:58 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/28 20:48:52 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

char	*ft_optimal_2(t_box *a, char *ptr, int i)
{
	if (ptr == NULL)
		ptr = a->final[i];
	if (ft_strlen(ptr) > ft_strlen(a->final[i]))
		ptr = a->final[i];
	return (ptr);
}

void	ft_optimal_3(t_box *a, char *ptr)
{
	ft_make_fresh(a, ptr);
	ft_change_matrix(a, ptr);
}

void	ft_make_optimal(t_box *a)
{
	int		i;
	int		size;
	int		j;
	char	**mass;
	char	*ptr;

	i = 0;
	size = 0;
	ptr = NULL;
	while (a->final[i])
	{
		j = 0;
		mass = ft_strsplit(a->final[i], '-');
		while (mass[j])
		{
			if (ft_atoi(mass[j]) == a->rooms)
				ptr = ft_optimal_2(a, ptr, i);
			j++;
		}
		ft_free(mass);
		i++;
	}
	if (ptr != NULL)
		ft_optimal_3(a, ptr);
}

int		ft_checker(char *s, int j)
{
	char	**mass;
	int		i;

	mass = ft_strsplit(s, '-');
	i = 0;
	while (mass[i])
	{
		if (ft_atoi(mass[i]) == j)
		{
			ft_free(mass);
			return (1);
		}
		i++;
	}
	ft_free(mass);
	return (0);
}

int		ft_check_range(t_box *a, int cell)
{
	t_links *tmp;

	tmp = a->begin;
	while (tmp)
	{
		if (tmp->num == cell)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}
