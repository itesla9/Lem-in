/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 14:30:56 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/29 19:17:50 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

char	**ft_make_anew_names(t_box *a, char **mass)
{
	char	**anew;
	int		size;
	int		i;

	i = 0;
	size = ft_count_double(a->names);
	anew = (char **)malloc(sizeof(char *) * (size + 2));
	while (i < size)
	{
		anew[i] = ft_strdup(a->names[i]);
		i++;
	}
	if (ft_check_name(a->names, mass[0]) == 1)
		a->error = 1;
	anew[i++] = ft_strdup(mass[0]);
	anew[i] = 0;
	ft_free(a->names);
	return (anew);
}

char	**ft_make_anew_coord(t_box *a, char **mass)
{
	char	**anew;
	int		size;
	int		i;

	i = 0;
	size = ft_count_double(a->coord);
	anew = (char **)malloc(sizeof(char *) * (size + 2));
	while (i < size)
	{
		anew[i] = ft_strdup(a->coord[i]);
		i++;
	}
	anew[i++] = ft_strjoin(mass[1], mass[2]);
	anew[i] = 0;
	ft_free(a->coord);
	return (anew);
}

char	**ft_make_anew_links(t_box *a, char *line)
{
	char	*s;
	char	**anew;
	int		size;
	int		i;

	i = 0;
	s = NULL;
	size = ft_count_double(a->links);
	anew = (char**)malloc(sizeof(char *) * (size + 2));
	while (i < size)
	{
		s = ft_epur(a->links[i]);
		anew[i] = ft_strdup(s);
		if (s != NULL) 
		free(s);
		i++;
	}
	s = ft_epur(line);
	anew[i++] = ft_strdup(s);
	anew[i] = 0;
	ft_free(a->links);
	if (s != NULL)
		free(s);
	return (anew);
}

char	**ft_make_chief(t_box *a, char *line)
{
	char	*s;
	char	**anew;
	int		size;
	int		i;

	i = 0;
	s = NULL;
	size = ft_count_double(a->chief);
	anew = (char **)malloc(sizeof(char *) * (size + 2));
	while (i < size)
	{
		s = ft_epur(a->chief[i]);
		anew[i] = ft_strdup(s);
		if (s != NULL)
		free(s);
		i++;
	}
	s = ft_epur(line);
	anew[i++] = ft_strdup(s);
	anew[i] = 0;
	ft_free(a->chief);
	if (s != NULL)
		free(s);
	return (anew);
}
