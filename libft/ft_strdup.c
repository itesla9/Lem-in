/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strdup.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 13:35:13 by yteslenk          #+#    #+#             */
/*   Updated: 2017/05/13 18:12:53 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strdup(const char *s1)
{
	int		i;
	int		j;
	char	*yy;

	i = 0;
	j = 0;
	while (s1[j])
		j++;
	yy = (char *)malloc(j + 1);
	if (yy == NULL)
		return (NULL);
	while (i < j)
	{
		yy[i] = s1[i];
		i++;
	}
	yy[i] = '\0';
	return (yy);
}
