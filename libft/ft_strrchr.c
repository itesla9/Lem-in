/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strrchr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 12:57:16 by yteslenk          #+#    #+#             */
/*   Updated: 2016/11/28 13:36:02 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strrchr(const char *s, int c)
{
	char	*final;

	final = NULL;
	while (*s)
	{
		if (*s == (char)c)
			final = (char *)s;
		s++;
		if (*s == (char)c)
			final = (char *)s;
	}
	return (final);
}
