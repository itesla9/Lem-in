/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   decision.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 13:57:39 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/22 12:45:14 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

void	ft_printf_lem(t_box *a, int l)
{
	int	size;

	size = a->ss[l][0].length - 1;
	while (size > 0)
	{
		if (a->ss[l][size].ant != 0)
			ft_printf("L%d-%s ", a->ss[l][size].ant, a->ss[l][size].name);
		size--;
	}
}

void	ft_move_struct(t_box *a, int l)
{
	int cell;
	int finish;

	finish = a->ss[l]->length - 1;
	cell = a->ss[l]->length - 1;
	while (cell >= 0)
	{
		if (cell < finish)
			a->ss[l][cell + 1].ant = a->ss[l][cell].ant;
		cell--;
	}
	if (a->counter != a->ants && a->ss[l][1].ant == 0)
	{
		a->counter++;
		a->ss[l][1].ant = a->counter;
	}
	if (a->ss[l][finish].ant != 0)
		a->fin++;
	ft_printf_lem(a, l);
}

void	ft_find_decision(t_box *a)
{
	int l;

	while (a->fin < a->ants)
	{
		l = 0;
		while (a->ss[l])
		{
			ft_move_struct(a, l);
			l++;
		}
		ft_printf("\n");
	}
}
