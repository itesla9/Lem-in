/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 14:33:20 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/28 18:13:40 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

int		ft_count_line_mass(char *line)
{
	int		i;
	int		size;
	char	**mass;

	i = 0;
	size = 0;
	mass = ft_strsplit(line, ' ');
	while (mass[i])
	{
		size++;
		i++;
	}
	ft_free(mass);
	return (size);
}

int		ft_count_double(char **mass)
{
	int	i;
	int size;

	i = 0;
	size = 0;
	while (mass[i])
	{
		size++;
		i++;
	}
	return (size);
}

void	ft_check_line(t_box *a, char *line)
{
	int		i;
	int		size;
	char	**mass;

	i = 0;
	size = 0;
	mass = ft_strsplit(line, ' ');
	while (mass[i])
	{
		size++;
		i++;
	}
	if (size == 1 && mass[0][0] != '#')
		a->flag++;
	ft_free(mass);
}

t_box	*ft_make_a(void)
{
	t_box *a;

	a = (t_box*)malloc(sizeof(t_box));
	a->flag = 0;
	a->ants = 0;
	a->error = 0;
	a->start = 0;
	a->end = 0;
	a->fin = 0;
	a->coord = NULL;
	a->names = NULL;
	a->chief = NULL;
	a->links = NULL;
	a->xy = NULL;
	a->final = NULL;
	a->fresh = NULL;
	a->b = NULL;
	a->z = NULL;
	a->counter = 0;
	return (a);
}
