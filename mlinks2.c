/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlinks2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/21 17:35:51 by yteslenk          #+#    #+#             */
/*   Updated: 2017/03/28 19:50:20 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

void	ft_make_fresh(t_box *a, char *s)
{
	char	**anew;
	int		size;
	int		z;

	if (a->fresh != NULL)
	{
		z = 0;
		size = ft_count_double(a->fresh);
		anew = (char **)malloc(sizeof(char *) * (size + 2));
		while (z < size)
		{
			anew[z] = ft_strdup(a->fresh[z]);
			z++;
		}
		anew[z++] = ft_strdup(s);
		anew[z] = 0;
		ft_free(a->fresh);
		a->fresh = anew;
	}
	if (a->fresh == NULL)
	{
		a->fresh = (char **)malloc(sizeof(char *) * 2);
		a->fresh[0] = ft_strdup(s);
		a->fresh[1] = 0;
	}
}

void	ft_matrix_to_change(t_box *a, t_l *tmp)
{
	while (tmp)
	{
		a->matrix[tmp->y][tmp->x] = 0;
		a->matrix[tmp->x][tmp->y] = 0;
		tmp = tmp->next;
	}
}

void	ft_forz(t_box *a, char **mass, int i)
{
	a->z = (t_l*)malloc(sizeof(t_l));
	a->z->y = ft_atoi(mass[i]);
	a->z->x = ft_atoi(mass[i + 1]);
	a->z->next = NULL;
}

t_l		*ft_change_matrix2(t_box *a, char **mass, int i, t_l *tmp)
{
	t_l *new;

	if (a->z != NULL)
	{
		new = (t_l*)malloc(sizeof(t_l));
		new->y = ft_atoi(mass[i]);
		new->x = ft_atoi(mass[i + 1]);
		new->next = NULL;
		tmp->next = new;
		tmp = tmp->next;
	}
	if (a->z == NULL)
	{
		a->z = (t_l*)malloc(sizeof(t_l));
		a->z->y = ft_atoi(mass[i]);
		a->z->x = ft_atoi(mass[i + 1]);
		a->z->next = NULL;
		tmp = a->z;
	}
	return (tmp);
}

void	ft_change_matrix(t_box *a, char *s)
{
	t_l		*tmp;
	char	**mass;
	int		size;
	int		i;

	i = 0;
	mass = ft_strsplit(s, '-');
	size = ft_count_double(mass);
	if (size <= 2)
		ft_forz(a, mass, i);
	if (size > 2)
	{
		while (i < size - 1)
		{
			if (a->z != NULL)
				tmp = ft_change_matrix2(a, mass, i, tmp);
			if (a->z == NULL)
				tmp = ft_change_matrix2(a, mass, i, tmp);
			i++;
		}
	}
	tmp = a->z;
	ft_matrix_to_change(a, tmp);
	free(a->z);
	a->z = NULL;
	ft_free(mass);
}
